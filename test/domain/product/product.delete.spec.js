const product = require('../../../src/domain/product/usecase.js')
const { expect } = require('chai')
const productRepository = require('../../infraestructure/db/productRepository.js')
const { exampleProduct } = require('../index.js')

describe('Product domain', () => {
  const productUseCase = product({ productRepository })
  describe('Delete product examples', () => {
    let productId
    beforeEach(() => {
      return productUseCase.createProduct({ product: exampleProduct })
        .then(productCreated => (productId = productCreated.id))
    })
    it('When delete existing product, the product can not be retrieved', () => {
      return productUseCase.deleteProduct({ id: productId })
        .then(() => productUseCase.getProduct({ id: productId }))
        .then(productDeleted => expect(productDeleted).to.be.equal(undefined))
    })
  })
})
