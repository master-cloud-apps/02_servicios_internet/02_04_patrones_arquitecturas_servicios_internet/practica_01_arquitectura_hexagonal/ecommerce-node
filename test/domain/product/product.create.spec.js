const product = require('../../../src/domain/product/usecase.js')
const { expect } = require('chai')
const productRepository = require('../../infraestructure/db/productRepository.js')
const { exampleProduct } = require('../index.js')

describe('Product domain', () => {
  const productUseCase = product({ productRepository })
  describe('Create product examples', () => {
    it('Should create a product', () => {
      return productUseCase
        .createProduct({ product: exampleProduct })
        .then((finalProduct) => expect(finalProduct.id).to.not.be.equal(undefined))
    })
    it('When product created, product can be found', () => {
      return productUseCase.createProduct({ product: exampleProduct })
        .then(({ id }) => productUseCase.getProduct({ id }))
        .then(product => expect(product.name).to.be.equal(exampleProduct.name))
    })
  })
})
