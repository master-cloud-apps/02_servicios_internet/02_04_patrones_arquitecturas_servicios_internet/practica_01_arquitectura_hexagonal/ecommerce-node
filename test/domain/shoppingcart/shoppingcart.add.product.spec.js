const shoppingCart = require('../../../src/domain/shoppingcart/usecase.js')
const { expect } = require('chai')
const shoppingCartRepository = require('./../../infraestructure/db/shoppingCartRepository.js')
const productRepository = require('../../infraestructure/db/productRepository.js')
const { exampleShoppingCart, exampleProduct } = require('../index.js')
const shoppingCartUseCase = shoppingCart({ shoppingCartRepository, productRepository })

describe('Shopping Cart domain', () => {
  describe('Add product to shopping cart examples', () => {
    it('Given product and cart created, when add product to shopping cart should include the product', () => {
      return addProductToCart(exampleProduct, exampleShoppingCart, 10)
        .then(shoppingCartUpdated => expect(shoppingCartUpdated.cartItems.length).to.be.equal(1))
    })
    it('Given product and cart created, when add product should be that product in the items of the cart', () => {
      return addProductToCart(exampleProduct, exampleShoppingCart, 10)
        .then(shoppingCartUpdated => {
          expect(shoppingCartUpdated.cartItems.length).to.be.equal(1)
          expect(shoppingCartUpdated.cartItems[0].quantity).to.be.equal(10)
          expect(shoppingCartUpdated.cartItems[0].product).to.include(exampleProduct)
        })
    })
    it('Given product included in cart, when add same product the quantity should change', () => {
      return addProductToCart(exampleProduct, exampleShoppingCart, 10)
        .then(cart => shoppingCartUseCase.addProductToShoppingCart({ cartId: cart.id, productId: cart.cartItems[0].product.id, quantity: 120 }))
        .then(shoppingCartUpdated => {
          expect(shoppingCartUpdated.cartItems.length).to.be.equal(1)
          expect(shoppingCartUpdated.cartItems[0].quantity).to.be.equal(120)
          expect(shoppingCartUpdated.cartItems[0].product).to.include(exampleProduct)
        })
    })
    it('Given three products created and a cart, it can be add the three products', () => {
      const createdThreeProductsPromiseArray = [productRepository.save(exampleProduct), productRepository.save(exampleProduct), productRepository.save(exampleProduct)]
      return Promise.all(createdThreeProductsPromiseArray)
        .then(products => shoppingCartUseCase.createShoppingCart({ shoppingCart: exampleShoppingCart })
          .then(cartCreated => Promise.all(products.map(product => shoppingCartUseCase.addProductToShoppingCart({ cartId: cartCreated.id, productId: product.id, quantity: 10 })))))
        .then(modifiedCarts => shoppingCartUseCase.getShoppingCart({ id: modifiedCarts[0].id }))
        .then(cart => {
          cart.cartItems.forEach(item => {
            expect(item.quantity).to.be.equal(10)
            expect(item.product).to.include(exampleProduct)
          })
          expect(cart.cartItems).to.be.an('array').that.have.lengthOf(3)
          expect(cart.status).to.be.equal(exampleShoppingCart.status)
        })
    })
  })
  describe('Add product to shopping cart error cases', () => {
    it('Given shoppingCart non existing, when add Product should raise error', () => {
      return shoppingCartUseCase.addProductToShoppingCart({ cartId: -100, productId: 0, quantity: 10 })
        .then(
          () => Promise.reject(new Error('Expect method to reject.')),
          error => {
            expect(error.errors).not.to.be.equal(undefined)
            expect(error.errors.message).to.be.equal('ShoppingCart not found')
          })
    })
    it('Given product non existing, when add Product to cart should raise error', () => {
      return shoppingCartUseCase.createShoppingCart({ shoppingCart: exampleShoppingCart })
        .then(cart => shoppingCartUseCase.addProductToShoppingCart({ cartId: cart.id, productId: -100, quantity: 10 }))
        .then(
          () => Promise.reject(new Error('Expect method to reject.')),
          error => {
            expect(error.errors).not.to.be.equal(undefined)
            expect(error.errors.message).to.be.equal('Product not found')
          })
    })
    it('Given product non existing, when add Product to cart should raise error', () => {
      return shoppingCartUseCase.createShoppingCart({ shoppingCart: exampleShoppingCart })
        .then(cart => shoppingCartUseCase.addProductToShoppingCart({ cartId: cart.id, productId: -100, quantity: 10 }))
        .then(
          () => Promise.reject(new Error('Expect method to reject.')),
          error => {
            expect(error.errors).not.to.be.equal(undefined)
            expect(error.errors.message).to.be.equal('Product not found')
          })
    })
  })
})

const addProductToCart = (product, cart, quantity) => {
  return createShoppingCartAndProduct(product, cart)
    .then(({ cartId, productId }) => shoppingCartUseCase.addProductToShoppingCart({ cartId, productId, quantity }))
}

const createShoppingCartAndProduct = (product, cart) => {
  return productRepository.save(product)
    .then(productCreated => productCreated.id)
    .then(productId => shoppingCartUseCase.createShoppingCart({ shoppingCart: cart })
      .then(cartCreated => ({
        cartId: cartCreated.id,
        productId
      })))
}
