
const carts = []
const save = (cart) => {
  const id = getCartId(cart)
  if (carts.findIndex(cartInner => cartInner.id === id) === -1) {
    carts.push({ id, ...cart })
  } else {
    carts[carts.findIndex(cartInner => cartInner.id === id)] = cart
  }
  return Promise.resolve(findProductById(id))
}
const getShoppingCart = (id) => Promise.resolve(findProductById(id))
const findProductById = id => carts.find(p => p.id === id)

const getCartId = cart => cart.id === undefined ? carts.length : cart.id

module.exports = {
  save,
  getShoppingCart
}
