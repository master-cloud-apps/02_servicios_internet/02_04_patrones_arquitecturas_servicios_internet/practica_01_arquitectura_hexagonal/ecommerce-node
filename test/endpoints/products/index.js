const { getIdFromResponseLocationHeader } = require('../index.js')
const request = require('supertest')
const { PRODUCTS_URI } = require('../../../src/app/endpoints/constants.js')

const exampleId = '6033994bc8baf639a235755f'

const createProduct = async (app, product) => {
  const responseProductCreated = await request(app)
    .post(PRODUCTS_URI)
    .send(product)

  const productId = getIdFromResponseLocationHeader(responseProductCreated)
  return productId
}

module.exports = {
  exampleId,
  createProduct
}
