const request = require('supertest')
const createApp = require('../../../src/app/index.js')
const appDependencies = require('../../../src/app/dependencies/index.js')
const { manageInMemoryDatabase } = require('../../db/index')
const { expect } = require('chai')
const { PRODUCTS_URI } = require('../../../src/app/endpoints/constants.js')

const app = createApp(appDependencies)

describe('Endpoints for Product', () => {
  describe(`POST ${PRODUCTS_URI}`, () => {
    manageInMemoryDatabase()
    it('When create product correctly should return 201', async () => {
      const response = await request(app)
        .post(PRODUCTS_URI)
        .send({ kind: 'Book', name: 'BookExample', description: 'Book Description' })

      expect(response.statusCode).to.be.equal(201)
      expect(response.headers.location).to.not.be.equal(undefined)
    })
    it('When create product without expected keys should return bad request', async () => {
      const response = await request(app)
        .post(PRODUCTS_URI)
        .send({ kind: 'Book', description: 'Book Description' })

      expect(response.statusCode).to.be.equal(400)
    })
    it('When create product without kind key should return bad request', async () => {
      const response = await request(app)
        .post(PRODUCTS_URI)
        .send({ name: 'Book', description: 'Book Description' })

      expect(response.statusCode).to.be.equal(400)
      expect(response.body.error.kind.name).to.be.equal('ValidatorError')
    })
    it('When create product without name key should return bad request', async () => {
      const response = await request(app)
        .post(PRODUCTS_URI)
        .send({ kind: 'Book', description: 'Book Description' })

      expect(response.statusCode).to.be.equal(400)
      expect(response.body.error.name.name).to.be.equal('ValidatorError')
    })
  })
})
