const request = require('supertest')
const createApp = require('../../../src/app/index.js')
const appDependencies = require('../../../src/app/dependencies/index.js')
const { manageInMemoryDatabase } = require('../../db/index')
const { expect } = require('chai')
const { getIdFromResponseLocationHeader } = require('../index.js')
const { PRODUCTS_URI } = require('../../../src/app/endpoints/constants.js')
const { exampleId } = require('./index.js')

const app = createApp(appDependencies)

describe('Endpoints for Product', () => {
  manageInMemoryDatabase()
  describe(`DELETE ${PRODUCTS_URI}/:productId`, () => {
    it('Given product created, when delete it, should return no content', async () => {
      const responseProductCreated = await request(app)
        .post(PRODUCTS_URI)
        .send({ kind: 'Book', name: 'BookExample', description: 'Book Description' })

      const productId = getIdFromResponseLocationHeader(responseProductCreated)

      const productResponse = await request(app).delete(`${PRODUCTS_URI}/${productId}`)

      expect(productResponse.status).to.be.equal(204)
    })
    it('When delete product not created, should return not found', async () => {
      const productResponse = await request(app).delete(`${PRODUCTS_URI}/${exampleId}`)

      expect(productResponse.status).to.be.equal(404)
    })
    it('When delete productId wrong, should return nbad request', async () => {
      const productResponse = await request(app).delete(`${PRODUCTS_URI}/1`)

      expect(productResponse.status).to.be.equal(400)
    })
    it('Given product deleted, when get product should return not found', async () => {
      const responseProductCreated = await request(app)
        .post(PRODUCTS_URI)
        .send({ kind: 'Book', name: 'BookExample', description: 'Book Description' })

      const productId = getIdFromResponseLocationHeader(responseProductCreated)

      const productDeleteResponse = await request(app).delete(`${PRODUCTS_URI}/${productId}`)
      const productGetResponse = await request(app).delete(`${PRODUCTS_URI}/${productId}`)

      expect(productDeleteResponse.status).to.be.equal(204)
      expect(productGetResponse.status).to.be.equal(404)
    })
  })
})
