const { SHOPPING_CARTS_URI } = require('../../../src/app/endpoints/constants')
const request = require('supertest')
const { getIdFromResponseLocationHeader } = require('../index.js')

const createCart = async (app, cart) => {
  const responseCartCreated = await request(app)
    .post(SHOPPING_CARTS_URI)
    .send(cart)

  const cartId = getIdFromResponseLocationHeader(responseCartCreated)
  return cartId
}

module.exports = {
  createCart
}
