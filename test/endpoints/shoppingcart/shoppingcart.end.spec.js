const request = require('supertest')
const createApp = require('../../../src/app/index.js')
const appDependencies = require('../../../src/app/dependencies/index.js')
const { manageInMemoryDatabase } = require('../../db/index')
const { expect } = require('chai')
const { SHOPPING_CARTS_URI } = require('../../../src/app/endpoints/constants.js')
const { exampleShoppingCart, exampleProduct } = require('../../domain/index.js')
const { createCart } = require('./index.js')
const { createProduct } = require('../products/index.js')
const { NOT_AVAILABLE_CODE } = require('../../../src/domain/model.js')

const app = createApp(appDependencies)

describe('Endpoints for ShoppingCart', () => {
  manageInMemoryDatabase()
  describe(`PATCH ${SHOPPING_CARTS_URI}/:cartId`, () => {
    let cartId
    let productId
    beforeEach(async () => {
      cartId = await createCart(app, exampleShoppingCart)
      productId = await createProduct(app, exampleProduct)
    })
    it('Given shopping cart with 2 products, end cart should return bad request', async () => {
      const productId2 = await createProduct(app, exampleProduct)

      const addProductInCartResponse = await request(app)
        .post(`${SHOPPING_CARTS_URI}/${cartId}/product/${productId}/quantity/10`)
      const addProductInCartResponse2 = await request(app)
        .post(`${SHOPPING_CARTS_URI}/${cartId}/product/${productId2}/quantity/10`)

      expect(addProductInCartResponse.status).to.be.equal(201)
      expect(addProductInCartResponse2.status).to.be.equal(201)

      const endCartResponse = await request(app)
        .patch(`${SHOPPING_CARTS_URI}/${cartId}`)

      expect(endCartResponse.status).to.be.equal(400)
      expect(endCartResponse.body.error.name).to.be.equal(NOT_AVAILABLE_CODE)
    })
    it('Given shopping cart with 1 product, end cart should return OK', async () => {
      const addProductInCartResponse = await request(app)
        .post(`${SHOPPING_CARTS_URI}/${cartId}/product/${productId}/quantity/10`)

      expect(addProductInCartResponse.status).to.be.equal(201)

      const endCartResponse = await request(app)
        .patch(`${SHOPPING_CARTS_URI}/${cartId}`)

      expect(endCartResponse.status).to.be.equal(200)
      expect(endCartResponse.body.data.status).to.be.equal('Completed')
    })
  })
})
