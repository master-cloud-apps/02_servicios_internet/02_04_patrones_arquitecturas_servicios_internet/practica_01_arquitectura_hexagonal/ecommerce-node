const request = require('supertest')
const createApp = require('../../../src/app/index.js')
const appDependencies = require('../../../src/app/dependencies/index.js')
const { manageInMemoryDatabase } = require('../../db/index')
const { expect } = require('chai')
const { SHOPPING_CARTS_URI, PRODUCTS_URI } = require('../../../src/app/endpoints/constants.js')
const { exampleShoppingCart, exampleProduct } = require('../../domain/index.js')
const { getIdFromResponseLocationHeader } = require('../index.js')
const { exampleId } = require('../products/index.js')

const app = createApp(appDependencies)

describe('Endpoints for ShoppingCart', () => {
  describe(`DELETE ${SHOPPING_CARTS_URI}/:cartId/products/:productId`, () => {
    manageInMemoryDatabase()
    it('Given shopping cart and product created, should be able to delete product', async () => {
      const responseCartCreated = await request(app)
        .post(SHOPPING_CARTS_URI)
        .send(exampleShoppingCart)
      const responseProductCreated = await request(app)
        .post(PRODUCTS_URI)
        .send(exampleProduct)

      const cartId = getIdFromResponseLocationHeader(responseCartCreated)
      const productId = getIdFromResponseLocationHeader(responseProductCreated)

      const addProductInCartResponse = await request(app)
        .post(`${SHOPPING_CARTS_URI}/${cartId}/product/${productId}/quantity/10`)

      expect(addProductInCartResponse.status).to.be.equal(201)

      const deleteProductInCartResponse = await request(app)
        .delete(`${SHOPPING_CARTS_URI}/${cartId}/product/${productId}`)

      expect(deleteProductInCartResponse.status).to.be.equal(204)

      const cartGetResponse = await request(app).get(`${SHOPPING_CARTS_URI}/${cartId}`)

      expect(cartGetResponse.status).to.be.equal(200)
      expect(cartGetResponse.body.data.status).to.be.equal(exampleShoppingCart.status)
      expect(cartGetResponse.body.data.id).to.not.be.equal(undefined)
      expect(cartGetResponse.body.data.cartItems).to.be.an('array').that.has.lengthOf(0)
    })
    it('Given cart not found, when delete product from cart should return not found', async () => {
      const deleteProductInCartResponse = await request(app)
        .delete(`${SHOPPING_CARTS_URI}/${exampleId}/product/${exampleId}`)

      expect(deleteProductInCartResponse.status).to.be.equal(404)
      expect(deleteProductInCartResponse.body.error.name).to.be.equal('NotFound')
      expect(deleteProductInCartResponse.body.error.entity).to.be.equal('ShoppingCart')
    })
    it('Given product not found, when delete product from cart should return no content', async () => {
      const responseCartCreated = await request(app)
        .post(SHOPPING_CARTS_URI)
        .send(exampleShoppingCart)

      const cartId = getIdFromResponseLocationHeader(responseCartCreated)
      const deleteProductInCartResponse = await request(app)
        .delete(`${SHOPPING_CARTS_URI}/${cartId}/product/${exampleId}`)

      expect(deleteProductInCartResponse.status).to.be.equal(204)
    })
  })
})
