const request = require('supertest')
const createApp = require('../../../src/app/index.js')
const appDependencies = require('../../../src/app/dependencies/index.js')
const { manageInMemoryDatabase } = require('../../db/index')
const { expect } = require('chai')
const { SHOPPING_CARTS_URI } = require('../../../src/app/endpoints/constants.js')
const { exampleShoppingCart } = require('../../domain/index.js')

const app = createApp(appDependencies)

describe('Endpoints for ShoppingCart', () => {
  describe(`POST ${SHOPPING_CARTS_URI}`, () => {
    manageInMemoryDatabase()
    it('When create product correctly should return 201', async () => {
      const response = await request(app)
        .post(SHOPPING_CARTS_URI)
        .send(exampleShoppingCart)

      expect(response.statusCode).to.be.equal(201)
      expect(response.headers.location).to.not.be.equal(undefined)
    })
    it('When create product without expected keys should return bad request', async () => {
      const response = await request(app)
        .post(SHOPPING_CARTS_URI)
        .send({ })

      expect(response.statusCode).to.be.equal(400)
    })
    it('When create product without kind key should return bad request', async () => {
      const response = await request(app)
        .post(SHOPPING_CARTS_URI)
        .send({ })

      expect(response.statusCode).to.be.equal(400)
      expect(response.body.error.status.name).to.be.equal('ValidatorError')
    })
  })
})
