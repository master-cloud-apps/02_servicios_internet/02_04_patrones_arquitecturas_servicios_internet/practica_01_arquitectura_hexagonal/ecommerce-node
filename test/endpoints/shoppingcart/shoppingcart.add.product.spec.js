const request = require('supertest')
const createApp = require('../../../src/app/index.js')
const appDependencies = require('../../../src/app/dependencies/index.js')
const { manageInMemoryDatabase } = require('../../db/index')
const { expect } = require('chai')
const { SHOPPING_CARTS_URI, PRODUCTS_URI } = require('../../../src/app/endpoints/constants.js')
const { exampleShoppingCart, exampleProduct } = require('../../domain/index.js')
const { getIdFromResponseLocationHeader } = require('../index.js')
const { exampleId } = require('../products/index.js')

const app = createApp(appDependencies)

describe('Endpoints for ShoppingCart', () => {
  describe(`POST ${SHOPPING_CARTS_URI}/:cartId/products/:productId/quantity/:prodQuantity`, () => {
    manageInMemoryDatabase()
    it('Given shopping cart and product created, should be able to add product', async () => {
      const responseCartCreated = await request(app)
        .post(SHOPPING_CARTS_URI)
        .send(exampleShoppingCart)
      const responseProductCreated = await request(app)
        .post(PRODUCTS_URI)
        .send(exampleProduct)

      const cartId = getIdFromResponseLocationHeader(responseCartCreated)
      const productId = getIdFromResponseLocationHeader(responseProductCreated)

      const addProductInCartResponse = await request(app)
        .post(`${SHOPPING_CARTS_URI}/${cartId}/product/${productId}/quantity/10`)

      expect(addProductInCartResponse.status).to.be.equal(201)

      const cartGetResponse = await request(app).get(`${SHOPPING_CARTS_URI}/${cartId}`)

      expect(cartGetResponse.status).to.be.equal(200)
      expect(cartGetResponse.body.data.status).to.be.equal(exampleShoppingCart.status)
      expect(cartGetResponse.body.data.id).to.not.be.equal(undefined)
      expect(cartGetResponse.body.data.cartItems).to.be.an('array').that.has.lengthOf(1)
      expect(cartGetResponse.body.data.cartItems[0].quantity).to.be.equal(10)
      expect(cartGetResponse.body.data.cartItems[0].product).to.include(exampleProduct)
    })
    it('Given shopping cart and product included, when add same product should update quantity', async () => {
      const responseCartCreated = await request(app)
        .post(SHOPPING_CARTS_URI)
        .send(exampleShoppingCart)
      const responseProductCreated = await request(app)
        .post(PRODUCTS_URI)
        .send(exampleProduct)

      const cartId = getIdFromResponseLocationHeader(responseCartCreated)
      const productId = getIdFromResponseLocationHeader(responseProductCreated)

      await request(app)
        .post(`${SHOPPING_CARTS_URI}/${cartId}/product/${productId}/quantity/10`)
      const addProductInCartResponse = await request(app)
        .post(`${SHOPPING_CARTS_URI}/${cartId}/product/${productId}/quantity/23`)

      expect(addProductInCartResponse.status).to.be.equal(201)

      const cartGetResponse = await request(app).get(`${SHOPPING_CARTS_URI}/${cartId}`)

      expect(cartGetResponse.status).to.be.equal(200)
      expect(cartGetResponse.body.data.status).to.be.equal(exampleShoppingCart.status)
      expect(cartGetResponse.body.data.id).to.not.be.equal(undefined)
      expect(cartGetResponse.body.data.cartItems).to.be.an('array').that.has.lengthOf(1)
      expect(cartGetResponse.body.data.cartItems[0].quantity).to.be.equal(23)
      expect(cartGetResponse.body.data.cartItems[0].product).to.include(exampleProduct)
    })
    it('Given no cart, when add product to cart should return not found', async () => {
      const addProductInCartResponse = await request(app)
        .post(`${SHOPPING_CARTS_URI}/${exampleId}/product/${exampleId}/quantity/23`)

      expect(addProductInCartResponse.status).to.be.equal(404)
      expect(addProductInCartResponse.body.error.entity).to.be.equal('ShoppingCart')
    })
    it('Given no product, when add product to cart should return not found', async () => {
      const responseCartCreated = await request(app)
        .post(SHOPPING_CARTS_URI)
        .send(exampleShoppingCart)
      const cartId = getIdFromResponseLocationHeader(responseCartCreated)
      const addProductInCartResponse = await request(app)
        .post(`${SHOPPING_CARTS_URI}/${cartId}/product/${exampleId}/quantity/23`)

      expect(addProductInCartResponse.status).to.be.equal(404)
      expect(addProductInCartResponse.body.error.entity).to.be.equal('Product')
    })
  })
})
