# Práctica 1 - Arquitectura Hexagonal

## Configuración

Para esta parte de la práctica que se implementa con Node y MongoDB, se necesita lo siguiente:

* Levantar un mongoDB en el puerto 27017 local.
  * La base de datos se llama `ecommerce`.
* Lanzar el script `npm start`

## Tests del dominio

Los tests del dominio los podemos ver en los siguientes ficheros:

* **Tests del dominio de producto**.
  * [Tests][1] de creación del producto.
  * [Tests][2] de borrado del producto.
* **Tests del dominio del carrito de la compra**.
  * [Tests][3] de creación del carrito.
  * [Tests][4] de añadir product al carrito.
  * [Tests][5] de finalizar el carrito.


## Notas de la implemnentación

En vez de realizar un servicio random que devuelva `true` o `false` aleatoriamente para el servicio de
validación del carrito, se ha implementado comprobando cuántos productos tiene el carrito. En caso
de tener un número par, devolverá un error y en caso de ser impart se completará correctamente el carrito.

Aquí dejamos el detalle del [servicio][6]:

```javascript
const isValid = cart => Promise.resolve(cart.cartItems.length % 2 !== 0)

module.exports = { isValid }
```


## Enunciado

Se desea implementar una aplicación de comercio electrónico. La aplicación proporciona dos casos de uso diferentes:

* CRUD de productos que los clientes podrán comprar
* Gestión del carrito de la compra:
    * Añadir productos al carrito
    * Eliminar productos del carrito
    * Eliminar el carrito
    * Finalizar el carrito. Cuando se finaliza el carrito, la aplicación debe validar el carrito, porque puede haber
      productos que ya no están disponibles. Para ello, la aplicación utiliza un servicio externo que se simula como un
      servicio Spring que simplemente devuelve aleatoriamente true o false dependiendo de si el carrito ha sido validado
      o no. En caso positivo, la operación de finalización del carrito termina con éxito, en caso contrario se notifica
      que hay productos no disponibles.

Estos casos de uso se proporcionan a través de una API REST con los siguientes endpoints:

* **Productos**:
    * GET /api/products - Muestra los productos
    * POST /api/products - Añade un producto
    * GET /api/products/:id - Muestra un producto en específico
    * DELETE /api/products/:id - Borra un producto en específico


* **ShoppingCart**:
    * POST /api/shoppingcarts - Crea un carrito de compra
    * PATCH /api/shoppingcarts/:id - Modifica el carrito de compra para pasar el estado a completo (finalizar el
      carrito)
    * GET /api/shoppingcarts/:id - Obtiene un carrito de compra específico
    * DELETE /api/shoppingcarts/:id - Borra un carrito de compra específico
    * POST /api/shoppingcarts/:cart_id/product/:prod_id/quantity/:prod_quantity - Añade un producto al carrito de
      compra, en la cantidad indicada por :prod_quantity. Si ya existiera lo modifica con la nueva cantidad.
    * DELETE /api/shoppingcarts/:cart_id/product/:prod_id - Borra un producto específico de un carrito de compra.

Uno de los requisitos a la hora de implementar esta aplicación es que se debe seguir la **arquitectura hexagonal**. Para
ello, se deben separar las clases en paquetes que representan los diferentes componentes de la aplicación. El dominio
debe quedar totalmente separado de cualquier tecnología.

**Se pide**:

* Una implementación basada en Spring, usando H2 para la persistencia (**4 pts**)
* Pruebas unitarias de (**1 pt**):
    * Crear y borrar producto
    * Crear carrito y añadir producto al carrito
* Una implementación basada en Node con Express, usando un motor de persistencia (relacional o no relacional) y el
  correspondiente ORM (sequalize, mongoose…) (**4 pts**)
* Pruebas unitarias de (**1 pt**):
    * Crear y borrar producto.
    * Crear carrito y añadir producto al carrito.


[1]: /test/domain/product/product.create.spec.js
[2]: /test/domain/product/product.delete.spec.js
[3]: /test/domain/shoppingcart/shoppingcart.create.spec.js
[4]: /test/domain/shoppingcart/shoppingcart.add.product.spec.js
[5]: /test/domain/shoppingcart/shoppingcart.end.spec.js
[6]: /src/infraestructure/service/shoppingcart.js