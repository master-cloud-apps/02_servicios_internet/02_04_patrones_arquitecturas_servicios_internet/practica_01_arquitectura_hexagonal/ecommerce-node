const express = require('express')
const { SHOPPING_CARTS_URI } = require('../endpoints/constants.js')
const { shoppingCarts } = require('../endpoints/index.js')

module.exports = (useCases) => {
  const router = new express.Router()
  const shoppingCartHandler = shoppingCarts(useCases)

  router.post(SHOPPING_CARTS_URI, shoppingCartHandler.post)
  router.get(`${SHOPPING_CARTS_URI}/:cartId`, shoppingCartHandler.getById)
  router.delete(`${SHOPPING_CARTS_URI}/:cartId`, shoppingCartHandler.deleteById)
  router.patch(`${SHOPPING_CARTS_URI}/:cartId`, shoppingCartHandler.endCart)
  router.post(`${SHOPPING_CARTS_URI}/:cartId/product/:productId/quantity/:quantity`, shoppingCartHandler.addProductToCart)
  router.delete(`${SHOPPING_CARTS_URI}/:cartId/product/:productId`, shoppingCartHandler.deleteProductFromCart)

  return router
}
