const product = require('./product.js')
const shoppingcart = require('./shoppingcart.js')

module.exports = {
  productRouter: product,
  shoppingCartRouter: shoppingcart
}
