const { getFullUrl } = require('../request')
const { manageError, checkModelNone } = require('../response')

const handlers = ({ shoppingCartUseCase }) => ({
  post: (req, res) => {
    return shoppingCartUseCase
      .createShoppingCart(createShoppingCartFromRequest(req))
      .then(cart => res.status(201).location(getFullUrl(req) + cart.id).send())
      .catch(error => manageError(error, res))
  },
  getById: (req, res) => {
    return shoppingCartUseCase.getShoppingCart({ id: req.params.cartId })
      .then(cart => checkModelNone(cart, res))
      .then(cart => res.status(200).send({ data: cart }))
      .catch(error => manageError(error, res))
  },
  endCart: (req, res) => {
    return shoppingCartUseCase.endCart({ id: req.params.cartId })
      .then(cart => res.status(200).send({ data: cart }))
      .catch(error => manageError(error, res))
  },
  deleteById: (req, res) => {
    return shoppingCartUseCase.getShoppingCart({ id: req.params.cartId })
      .then(cart => checkModelNone(cart, res))
      .then(({ id }) => shoppingCartUseCase.deleteShoppingCart({ id }))
      .then(_ => res.sendStatus(204))
      .catch(error => manageError(error, res))
  },
  addProductToCart: (req, res) => {
    return shoppingCartUseCase.addProductToShoppingCart({ ...req.params })
      .then(_ => res.sendStatus(201))
      .catch(error => manageError(error, res))
  },
  deleteProductFromCart: (req, res) => {
    return shoppingCartUseCase.deleteProductFromShoppingCart({ ...req.params })
      .then(_ => res.sendStatus(204))
      .catch(error => manageError(error, res))
  }
})

module.exports = handlers

const createShoppingCartFromRequest = request => ({
  shoppingCart: {
    status: request.body.status
  }
})
