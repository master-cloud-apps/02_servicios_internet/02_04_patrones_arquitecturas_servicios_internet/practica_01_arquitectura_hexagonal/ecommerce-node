const PRODUCTS_URI = '/api/products'
const SHOPPING_CARTS_URI = '/api/shoppingcarts'

module.exports = {
  PRODUCTS_URI,
  SHOPPING_CARTS_URI
}
