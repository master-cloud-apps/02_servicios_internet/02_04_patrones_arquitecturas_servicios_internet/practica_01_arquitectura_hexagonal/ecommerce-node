module.exports = ({ productRepository }) => ({
  createProduct: ({ product }) => productRepository.save(product),
  getProduct: ({ id }) => productRepository.getProduct(id),
  getProducts: () => productRepository.getProducts(),
  deleteProduct: ({ id }) => productRepository.deleteProduct(id)
})
