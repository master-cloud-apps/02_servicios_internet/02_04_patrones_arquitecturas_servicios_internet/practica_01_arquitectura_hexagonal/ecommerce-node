const { checkEntityNone } = require('../model')
const { validateCartOrThrow, includeProductInShoppingCart, deleteProductFromCart } = require('./index.js')

module.exports = ({ shoppingCartRepository, productRepository, shoppingCartService }) => ({
  createShoppingCart: ({ shoppingCart }) => shoppingCartRepository.save({ ...shoppingCart, cartItems: [] }),

  getShoppingCart: ({ id }) => shoppingCartRepository.getShoppingCart(id),

  deleteShoppingCart: ({ id }) => shoppingCartRepository.deleteShoppingCart(id),

  endCart: ({ id }) => {
    return shoppingCartRepository.getShoppingCart(id)
      .then(cart => checkEntityNone(cart, 'ShoppingCart'))
      .then(cart => shoppingCartService.isValid(cart)
        .then(isCartValid => validateCartOrThrow(cart, isCartValid)))
      .then(cartCompleted => shoppingCartRepository.save(cartCompleted))
  },

  addProductToShoppingCart: ({ cartId, productId, quantity }) => {
    return shoppingCartRepository.getShoppingCart(cartId)
      .then(cart => checkEntityNone(cart, 'ShoppingCart'))
      .then(cartRetrieved => productRepository.getProduct(productId)
        .then(product => checkEntityNone(product, 'Product'))
        .then(productRetrieved => includeProductInShoppingCart(cartRetrieved, productRetrieved, quantity)))
      .then(finalCart => shoppingCartRepository.save(finalCart))
  },

  deleteProductFromShoppingCart: ({ cartId, productId }) => {
    return shoppingCartRepository.getShoppingCart(cartId)
      .then(cart => checkEntityNone(cart, 'ShoppingCart'))
      .then(cart => deleteProductFromCart(cart, productId))
      .then(cartUpdated => shoppingCartRepository.save(cartUpdated))
  }

})
