const mongoose = require('mongoose')
const { removeMongooseElements } = require('../../mongo.js')

const productSchema = new mongoose.Schema({
  kind: { type: String, required: true },
  name: { type: String, required: true },
  description: { type: String, required: true }
})

productSchema.methods.toJSON = function () {
  return removeMongooseElements(this.toObject())
}

module.exports = mongoose.model('Product', productSchema)
